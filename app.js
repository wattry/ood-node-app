// a minimal node app that displays your running processes on the OOD web host
var http = require('http');

var server = http.createServer(function (request, response) {
        response.writeHead(200, {
            "Content-Type": "text/html"
        });
        response.write("<h2>" + new Date() + "</h2>");
        response.write("<pre>" + process.version + "</pre>\n");
        response.end();
});
server.listen(3000);